package com.nust.eme.nust_app;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.nust.eme.nust_app.NavActivities.NavAttendanceActivity;
import com.nust.eme.nust_app.NavActivities.NavCourses.NavCoursesActivity;
import com.nust.eme.nust_app.NavActivities.NavFinances;
import com.nust.eme.nust_app.NavActivities.NavNotifications;
import com.nust.eme.nust_app.NavActivities.NavResults.NavResultsActivity;
import com.nust.eme.nust_app.NavActivities.NavTimetable.NavTimetable;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.z_toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);


        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        CollapsingToolbarLayout ctl = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        //ctl.setTitle("Tayyaba Taimur");


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_attendance) {
            Intent intent=new Intent(MainActivity.this, NavAttendanceActivity.class);
            startActivity(intent);
            // Handle the camera action
        } else if (id == R.id.nav_code) {

        } else if (id == R.id.nav_enrolled) {
            Intent intent=new Intent(MainActivity.this, NavCoursesActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_feedback) {

        } else if (id == R.id.nav_finance) {
            Intent intent=new Intent(MainActivity.this, NavFinances.class);
            startActivity(intent);

        } else if (id == R.id.nav_notifs) {
            Intent intent=new Intent(MainActivity.this, NavNotifications.class);
            startActivity(intent);
        }
        else if (id == R.id.nav_profile) {

        }
        else if (id == R.id.nav_results) {
            Intent intent=new Intent(MainActivity.this, NavResultsActivity.class);
            startActivity(intent);

        }
        else if (id == R.id.nav_timetable) {
            Intent intent=new Intent(MainActivity.this, NavTimetable.class);
            startActivity(intent);

        }
        else if (id == R.id.nav_videos) {

        }



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
