package com.nust.eme.nust_app.NavActivities.NavTimetable;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Toast;

import com.greasemonk.timetable.IGridItem;
import com.greasemonk.timetable.TimeRange;

import java.util.Calendar;
import java.util.Date;
import java.util.Random;

/**
 * Created by tayyabataimur on 1/12/18.
 */

public class PlanItems implements IGridItem
{
    private String employeeName, projectName;
    private TimeRange timeRange;


    public PlanItems() {}

    public PlanItems(Context context, String employeeName, String projectName, Date planStart, Date planEnd)
    {
        this.employeeName = employeeName;
        this.projectName = projectName;
        this.timeRange = new TimeRange(planStart, planEnd);
    }

    public static PlanItems generateSample(Context context,int i)
    {
        int index=0;
        final String[] firstNameSamples = {"8am", "9am", "10am", "11am", "12pm","1pm","2pm","3pm","4pm"};
        final String[] projectNames = {"DSD", "EPM", "DC","SE","FYP","AI"};

        // Generate a date range between now and 30 days
        Random rand = new Random();
        Calendar start = Calendar.getInstance();
        Calendar end = Calendar.getInstance();
        int r1 = -rand.nextInt(12);
        int r2 = rand.nextInt(12);
        start.add(Calendar.DATE, r1);
        end.add(Calendar.DATE, r2);


        return new PlanItems(context, firstNameSamples[i],
                projectNames[rand.nextInt(projectNames.length)],
                start.getTime(),
                end.getTime());
    }


    @Override
    public TimeRange getTimeRange()
    {
        return timeRange;
    }

    @Override
    public String getName()
    {
        return projectName;
    }

    @Override
    public String getPersonName()
    {
        return employeeName;
    }

    public int getItemColor()
    {
        return Color.argb(48, 0,0,255);
    }
}