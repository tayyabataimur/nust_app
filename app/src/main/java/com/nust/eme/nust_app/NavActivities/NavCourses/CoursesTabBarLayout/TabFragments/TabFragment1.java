package com.nust.eme.nust_app.NavActivities.NavCourses.CoursesTabBarLayout.TabFragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nust.eme.nust_app.R;

/**
 * Created by tayyabataimur on 1/12/18.
 */

public class TabFragment1 extends android.support.v4.app.Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_tab1, container, false);
    }
}
