package com.nust.eme.nust_app.NavActivities.NavResults.ResultsTabBarLayout.TabFragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nust.eme.nust_app.R;

/**
 * Created by tayyabataimur on 1/13/18.
 */

public class FragAssg extends android.support.v4.app.Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.frag_asssg, container, false);
    }
}
