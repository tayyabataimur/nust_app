package com.nust.eme.nust_app.NavActivities.NavResults.ResultsTabBarLayout;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.nust.eme.nust_app.NavActivities.NavCourses.CoursesTabBarLayout.TabFragments.TabFragment1;
import com.nust.eme.nust_app.NavActivities.NavCourses.CoursesTabBarLayout.TabFragments.TabFragment2;
import com.nust.eme.nust_app.NavActivities.NavResults.ResultsTabBarLayout.TabFragments.FragAssg;
import com.nust.eme.nust_app.NavActivities.NavResults.ResultsTabBarLayout.TabFragments.FragLab;
import com.nust.eme.nust_app.NavActivities.NavResults.ResultsTabBarLayout.TabFragments.FragQuiz;
import com.nust.eme.nust_app.NavActivities.NavResults.ResultsTabBarLayout.TabFragments.FragSessional;

/**
 * Created by tayyabataimur on 1/13/18.
 */

public class Adapter_res extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public Adapter_res(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                FragAssg tab1 = new FragAssg();
                return tab1;
            case 1:
                FragLab tab2 = new FragLab();
                return tab2;

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}