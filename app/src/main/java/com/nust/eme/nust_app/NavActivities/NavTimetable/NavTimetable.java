package com.nust.eme.nust_app.NavActivities.NavTimetable;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

import com.greasemonk.timetable.TimeTable;
import com.nust.eme.nust_app.R;

import java.util.ArrayList;
import java.util.List;

public class NavTimetable extends AppCompatActivity {

    private static final int GENERATED_AMOUNT = 9;
    private TimeTable timeTable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nav_timetable);
        getSupportActionBar().setTitle("Timetable");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        timeTable = (TimeTable) findViewById(R.id.time_table);
        timeTable.setItems(generateSamplePlanData(this));
    }

    private static List<PlanItems> generateSamplePlanData(Context context)
    {
        List<PlanItems> planItems = new ArrayList<>();
        for(int i = 0; i < GENERATED_AMOUNT; i++)
            planItems.add(PlanItems.generateSample(context,i));

        return planItems;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }
}
